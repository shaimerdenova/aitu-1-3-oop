package kz.aitu.oop.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//import javax.persistence.Entity;
//import javax.persistence.Id;

//@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    //@Id
    private int id;
    private String name;
    private int age;
    private double point;

    private String group;


    public String toString() {
        return "Student{" + "name='" + name + '\'' + ", age=" + age + ", point=" + point + '}';
    }

    public void setGroup(String next) {
    }

    public void setName(String next) {
    }

    public void setAge(int nextInt) {
    }

    public void setPoint(double nextDouble) {
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public double getPoint() {
        return point;
    }

    public String getGroup() {
        return group;
    }
}
