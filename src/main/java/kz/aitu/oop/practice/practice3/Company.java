package kz.aitu.oop.practice.practice3;

public class Company {

private int salary;

public Company(int salary){
    this.salary=salary;
}

public void setSalary(int salary){
    this.salary=salary;
}

public int getSalary(){
    return salary;
}
}
