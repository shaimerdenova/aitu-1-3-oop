package kz.aitu.oop.practice.practice4;


public class Aquarium {
    private int amountOfFish;
    private int amountOfReptilies;
    private int priceOfAquarium;

    private Aquarium() {
    }

    private static Aquarium myInput = null;

    public static Aquarium getSingleton() {
        if(myInput == null) myInput = new Aquarium();
        return myInput;
    }

    @Override
    public String toString() {
        return "There are " +amountOfFish + " fish and " +
               amountOfReptilies +
                " reptilies in your aquarium!!!";
    }

    public Aquarium(int amountOfFish, int amountOfReptilies, int priceOfAquarium){
        this.amountOfFish=amountOfFish;
        this.amountOfReptilies=amountOfReptilies;
        this.priceOfAquarium=priceOfAquarium;
    }

    public void setAmountOfFish(int amountOfFish){
        this.amountOfFish=amountOfFish;
    }

    public int getAmountOfFish(){
        return amountOfFish;
    }

    public void setAmountOfReptilies(int amountOfReptilies){
        this.amountOfReptilies=amountOfReptilies;
    }

    public int getAmountOfReptilies(){
        return amountOfReptilies;
    }

    public void setPriceOfAquarium(int priceOfAquarium){
        this.priceOfAquarium=priceOfAquarium;
    }

    public int getPriceOfAquarium(){
        return priceOfAquarium;
    }
}
