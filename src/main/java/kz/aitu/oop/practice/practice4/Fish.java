package kz.aitu.oop.practice.practice4;

public class Fish extends Aquarium {
    private String nameOfFish;
    private int priceOfFish;

    public Fish(int amountOfFish, int amountOfReptilies, int priceOfAquarium, String nameOfFish, int priceOfFish){
        super(amountOfFish, amountOfReptilies, priceOfAquarium);
        this.nameOfFish=nameOfFish;
        this.priceOfFish=priceOfFish;
    }

    public void setNameOfFish(String nameOfFish){
        this.nameOfFish=nameOfFish;
    }

    public String getNameOfFish(){
        return nameOfFish;
    }

    public void setPriceOfFish(int priceOfFish){
        this.priceOfFish=priceOfFish;
    }

    public int getPriceOfFish(){
        return priceOfFish;
    }

    public int getTotalPriceOfFish(){
        int totalPrice;
        totalPrice = super.getAmountOfFish()*priceOfFish;
        return totalPrice;
    }
}
