package kz.aitu.oop.practice.practice4;

public class Reptilies extends Aquarium{

    private String nameOfReptilia;
    private int priceOfReptilia;

    public Reptilies(int amountOfFish, int amountOfReptilies, int priceOfAquarium, String nameOfReptilia, int priceOfReptilia){
        super(amountOfFish, amountOfReptilies, priceOfAquarium);
        this.nameOfReptilia=nameOfReptilia;
        this.priceOfReptilia=priceOfReptilia;
    }

    public void setNameOfReptilia(String nameOfReptilia){
        this.nameOfReptilia=nameOfReptilia;
    }

    public String getNameOfReptilia(){
        return nameOfReptilia;
    }

    public void setPriceOfReptilia(int priceOfReptilia){
        this.priceOfReptilia=priceOfReptilia;
    }

    public int getPriceOfReptilia(){
        return priceOfReptilia;
    }

    public int getTotalPriceOfReptilies(){
        int totalPrice;
        totalPrice = super.getAmountOfReptilies()*priceOfReptilia;
        return totalPrice;
    }
}
