package kz.aitu.oop.practice.practice4;

import kz.aitu.oop.examples.practice6.Singleton;

public class main {
    public static void main(String[] args) {
        int totalPrice;
        Aquarium a = new Aquarium(5, 3, 20000);
        Fish f = new Fish(5, 3, 20000, "Nemo", 3200);
        Reptilies r = new Reptilies(5, 3, 20000, "Snake", 1800);

        System.out.println("Price of one fish which called " + f.getNameOfFish()+" is "+ f.getPriceOfFish() + ". Total price of "
        + a.getAmountOfFish() + " fish is "+f.getTotalPriceOfFish());

        System.out.println("Price of one reptilia which called " + r.getNameOfReptilia()+" is "+ r.getPriceOfReptilia() + ". Total price of "
                + a.getAmountOfReptilies() + " reptilies is "+r.getTotalPriceOfReptilies());

        totalPrice = a.getPriceOfAquarium()+f.getTotalPriceOfFish()+r.getTotalPriceOfReptilies();
        System.out.println("Total price of aquarium with all reptilies and fish is "+totalPrice);

        System.out.println(a.toString());


    }
}
