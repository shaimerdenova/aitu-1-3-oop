package kz.aitu.oop.practice.practice2;


// voditel and konduktora
public class Locomotive extends Train implements  TrainSystem{
    private int numberOfDrivers;
    private int numberOfConductors;
    private double totalCarryingCapacity;

    public Locomotive(){
        numberOfDrivers=2;
        numberOfConductors=12; // 1 conductor in 1 carriage
    }

    public void setNumberOfDrivers(int numberOfDrivers){
        this.numberOfDrivers=numberOfDrivers;
    }

    public int getNumberOfDrivers(){
        return numberOfDrivers;
    }

    public void setNumberOfConductors(int numberOfConductors){
        this.numberOfConductors=numberOfConductors;
    }

    public int getNumberOfConductors(){
        return numberOfConductors;
    }

    Carriage c = new Carriage();
    public double getTotalCarryingCapacity(){
        totalCarryingCapacity= super.getCarrying_capacity()*c.getNumberOfCarriages();
        return totalCarryingCapacity;
    }

    @Override
    public String getType(){

        return "Vagon for drivers and conductors";
    }
}
