package kz.aitu.oop.practice.practice2;

public class TrainFactory {
    public TrainSystem getTrain(main.vagonType type) {
        TrainSystem toReturn = null;
        switch (type) {
            case Carriage:
                toReturn = new Carriage();
                break;
            case Locomotive:
                toReturn = new Locomotive();
                break;
            default:
                throw new IllegalArgumentException("You can choose only two types of vagons: carriage or locomotive!!! " + type);
        }
        return toReturn;
    }
}
