package kz.aitu.oop.practice.practice2;

public class main {
    public enum vagonType {
        Carriage,
        Locomotive
    }
    public static void main(String[] args) {
        int totalPeople;
        Train t = new Train();
        Carriage c = new Carriage();
        Locomotive l = new Locomotive();
        totalPeople = l.getNumberOfConductors() + l.getNumberOfDrivers() + c.getTotalNumberOfPassengers();
        System.out.println("There are "+ t.getNumberOfPassengers() + " passengers in one carriage. Total number of passengers in this train is "+ c.getTotalNumberOfPassengers());
        System.out.println("Carrying capacity of one carriage is " + t.getCarrying_capacity()+". Total carrying capacity of the train with " + c.getNumberOfCarriages() + " carriages is " + l.getTotalCarryingCapacity());
        System.out.println("Total number of people in this train is "+ totalPeople + " (including drivers and conductors)");

        Train t2 = new Train(45, 110.0);
        System.out.println("There are "+ t2.getNumberOfPassengers() + " passengers in one carriage.");
        System.out.println("Carrying capacity of one carriage is " + t2.getCarrying_capacity());


        TrainFactory factory = new TrainFactory();

        TrainSystem carriage = factory.getTrain(vagonType.Carriage);
        TrainSystem locomotive= factory.getTrain(vagonType.Locomotive);

        System.out.println("Carriage is a " + carriage.getType());
        System.out.println("Locomotive is a " +  locomotive.getType());
    }
}
