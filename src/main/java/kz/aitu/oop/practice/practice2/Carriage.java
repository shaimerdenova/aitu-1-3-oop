package kz.aitu.oop.practice.practice2;

public class Carriage extends Train implements TrainSystem{
private int numberOfCarriages;

    public Carriage() {
        numberOfCarriages = 12;
    }

    public void setNumberOfCarriages(int numberOfCarriages){
    this.numberOfCarriages=numberOfCarriages;
}

  public int getNumberOfCarriages(){
      return numberOfCarriages;
  }

  public Carriage(int numberOfPassengers, double carrying_capacity, int numberOfCarriages){
      super(numberOfPassengers, carrying_capacity);
      this.numberOfCarriages=numberOfCarriages;
  }

  public int getTotalNumberOfPassengers(){
      int totalPassengers;
      totalPassengers = numberOfCarriages*super.getNumberOfPassengers();
      return totalPassengers;
  }

    @Override
    public String getType(){

        return "Vagon for passengers";
    }

}
