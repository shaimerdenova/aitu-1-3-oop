package kz.aitu.oop.practice.practice2;

public class Train {
    private  int numberOfPassengers;
    private double carrying_capacity;

    public Train(int numberOfPassengers, double carrying_capacity) {
        this.numberOfPassengers=numberOfPassengers;
        this.carrying_capacity=carrying_capacity;
    }

    public Train() {
        numberOfPassengers=50;
        carrying_capacity=130.0;
    }

    public void setNumberOfPassengers(int numberOfPassengers){
        this.numberOfPassengers=numberOfPassengers;
    }

    public int getNumberOfPassengers(){
        return numberOfPassengers;
    }

    public void setCarrying_capacity(double carrying_capacity){
        this.carrying_capacity=carrying_capacity;
    }

    public double getCarrying_capacity(){
        return carrying_capacity;
    }
}
