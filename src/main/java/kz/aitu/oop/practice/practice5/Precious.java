package kz.aitu.oop.practice.practice5;

public class Precious extends Necklace{
    private int price;
    private double weight;

    public Precious(int numberOfPrecious, int numberOfSemi, int price, double weight){
        super(numberOfPrecious, numberOfSemi);
        this.price=price;
        this.weight=weight;
    }

    public void setPrice(int price){
        this.price=price;
    }

    public int getPrice(){
        return price;
    }

    public void setWeight(double weight){
        this.weight=weight;
    }

    public double getWeight(){
        return weight;
    }

    public int getTotalPrice(){
        int totalPrice;
        totalPrice = super.getNumberOfPrecious()*price;
        return totalPrice;
    }

    public double getTotalWeight(){
        double totalWeight;
        totalWeight = super.getNumberOfPrecious()*weight;
        return totalWeight;
    }

}


