package kz.aitu.oop.practice.practice5;

public class Necklace {
    private String name;
    private boolean hasPrecious;
    private boolean hasSemi;
    private int numberOfPrecious;
    private int numberOfSemi;


    public Necklace(String name) {
        this.name = name;
    }



    public Necklace(String name, boolean hasPrecious, boolean hasSemi) {
        this.name = name;
        this.hasPrecious =hasPrecious;
        this.hasSemi =hasSemi;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public boolean isHasPrecious() {
        return hasPrecious;
    }

    public void setHasPrecious(boolean hasPrecious) {
        this.hasPrecious = hasPrecious;
    }

    public boolean isHasSemi() {
        return hasSemi;
    }

    public void setHasSemi(boolean semi) {
        this.hasSemi = hasSemi;
    }

    public Necklace(int numberOfPrecious, int numberOfSemi){
        this.numberOfPrecious=numberOfPrecious;
        this.numberOfSemi=numberOfSemi;
    }

    public void setNumberOfPrecious(int numberOfPrecious){

        this.numberOfPrecious=numberOfPrecious;
    }

    public int getNumberOfPrecious(){

        return numberOfPrecious;
    }

    public void setNumberOfSemi(int numberOfSemi){

        this.numberOfSemi=numberOfSemi;
    }

    public int getNumberOfSemi(){

        return numberOfSemi;
    }

}
