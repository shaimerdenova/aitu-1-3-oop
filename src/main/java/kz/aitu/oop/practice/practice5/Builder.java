package kz.aitu.oop.practice.practice5;

public class Builder {
    private Necklace necklace;

    public Builder(String name) {
        necklace = new Necklace(name);
    }


    public Builder addPrecious() {
        necklace.setHasPrecious(true);
        return this;
    }

    public Builder addSemi() {
        necklace.setHasSemi(true);
        return this;
    }

    public Necklace makeNecklace() {

        return necklace;
    }
}
