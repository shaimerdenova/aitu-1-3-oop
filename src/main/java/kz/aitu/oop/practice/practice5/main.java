package kz.aitu.oop.practice.practice5;

public class main {


    public static void main(String[] args) {
        Necklace n = new Necklace(10, 15);
        Precious p = new Precious(10, 15, 5500, 130);
        Semi s = new Semi(10, 15, 3000, 150);


        int totalPrice;
        totalPrice = p.getTotalPrice() + s.getTotalPrice();
        double totalWeight;
        totalWeight = p.getTotalWeight() + s.getTotalWeight();


        System.out.println("The price of one precious stone is " + p.getPrice() + ". Total price of " + n.getNumberOfPrecious() + " precious stones is " + p.getTotalPrice());
        System.out.println("The price of one semi stone is " + s.getPrice() + ". Total price of " + n.getNumberOfSemi() + " semi stones is " + s.getTotalPrice());
        System.out.println("Total price of this necklace is " + totalPrice);
        System.out.println("The weight of one precious stone is " + p.getWeight() + ". Total weight of " + n.getNumberOfPrecious() + " precious stones is " + p.getTotalWeight());
        System.out.println("The weight of one semi stone is " + s.getWeight() + ". Total weight of " + n.getNumberOfSemi() + " semi stones is " + s.getTotalWeight());
        System.out.println("Total weight of this necklace is " + totalWeight);

        Builder builder1 = new Builder(" NEW Necklace");
        Necklace Necklace = builder1
                .addPrecious()
                .addSemi()
                .makeNecklace();

    }

    }

