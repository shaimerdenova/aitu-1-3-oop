package kz.aitu.oop.controller;
import kz.aitu.oop.entity.Student;
import kz.aitu.oop.repository.StudentFileRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static java.lang.Integer.parseInt;
import java.io.FileNotFoundException;

//import kz.aitu.oop.repository.StudentDBRepository;

@RestController
@RequestMapping("/api/task/4")
@AllArgsConstructor
public class AssignmentController4 {

    /**
     *
     * @param group
     * @return all student name by group name
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}")
    public ResponseEntity<?> getStudentsByGroup(@PathVariable("group") String group) throws FileNotFoundException {

        //write your code here
        String result = "";
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        for (Student student: studentFileRepository.getStudents()) {
            String names = "";
                names += student.getName() + "</br>";
            }
        return ResponseEntity.ok(result);
    }

    /**
     *
     * @param group
     * @return stats by point letter (counting points): example  A-3, B-4, C-1, D-1, F-0
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}/stats")
    public ResponseEntity<?> getGroupStats(@PathVariable("group") String group) throws FileNotFoundException {

        //write your code here
        String result = "";
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        int A = 0, B=0, C=0, D=0,FX=0, F=0;
        int groupName;
        for (Student student: StudentFileRepository.getStudents()) {
                if(student.getPoint()>=0 && student.getPoint()<=29){
                    F++;
                }
                if(student.getPoint()>=30 && student.getPoint()<=49){
                    FX++;
                }
                if(student.getPoint()>=50 && student.getPoint()<=59){
                    D++;
                }
                if(student.getPoint()>=60 && student.getPoint()<=79){
                    C++;
                }
                if(student.getPoint()>=80 && student.getPoint()<=89){
                    B++;
                }
                if(student.getPoint()>=90 && student.getPoint()<=100){
                    A++;
                }

            }
        result= result+"Amount of A"+A+"  "+"Amount of B"+B+"  "+"Amount of C"+C+"  "+"Amount of D"+D+"  "+"Amount of FX"+FX+"  "+"Amount of F"+F;

        return ResponseEntity.ok(result);
    }

    /**
     *
     * @return top 5 students name by point
     * @throws FileNotFoundException
     */
    @GetMapping("/students/top")
    public ResponseEntity<?> getTopStudents() throws FileNotFoundException {

        //write your code here
        String result = "";
        StudentFileRepository StudentFileRepository = new StudentFileRepository();

        return ResponseEntity.ok(result);
    }

}

