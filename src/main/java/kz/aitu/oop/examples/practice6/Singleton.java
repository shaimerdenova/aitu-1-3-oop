package kz.aitu.oop.examples.practice6;

public class Singleton {
    public String str;


    private Singleton() {
    }
    private static Singleton myInput = null;

    public static Singleton getSingleton() {
        if(myInput == null) myInput = new Singleton();
        return myInput;
    }

    @Override
    public String toString() {
        return "Hello I am a singleton! " +
                "Let me say " + str +
                " to you!";
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }


}
