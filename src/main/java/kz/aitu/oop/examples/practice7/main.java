package kz.aitu.oop.examples.practice7;


public class main {
    public enum FoodType {
        CAKE,
        PIZZA
    }

    public static void main(String[] args) {
        FoodFactory factory = new FoodFactory();

        Food cake = factory.getFood(FoodType.CAKE);
        Food pizza= factory.getFood(FoodType.PIZZA);

        System.out.println("Cake");
        System.out.println("Someone ordered a " +   cake.getType());
        System.out.println("Pizza");
        System.out.println("Someone ordered a " +  pizza.getType());
    }
}
