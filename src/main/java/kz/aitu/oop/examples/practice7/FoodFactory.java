package kz.aitu.oop.examples.practice7;


public class FoodFactory {
    public Food getFood(main.FoodType type) {
        Food toReturn = null;
        switch (type) {
            case CAKE:
                toReturn = new Cake();
                break;
            case PIZZA:
                toReturn = new Pizza();
                break;
            default:
                throw new IllegalArgumentException("You can choose only two foods: cake or pizza!!! " + type);
        }
        return toReturn;
    }
}
