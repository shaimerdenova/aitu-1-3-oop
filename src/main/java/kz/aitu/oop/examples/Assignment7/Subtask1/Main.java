package kz.aitu.oop.examples.Assignment7.Subtask1;

public class Main {

    public static void main(String[] args) {
        Shape shape = new Shape("red", true) {
            @Override
            public double getArea() {
                return 0;
            }

            @Override
            public double getPerimeter() {
                return 0;
            }

            @Override
            public double getSide() {
                return 0;
            }
        };
        Shape shape2 = new Shape() {
            @Override
            public double getArea() {
                return 0;
            }

            @Override
            public double getPerimeter() {
                return 0;
            }

            @Override
            public double getSide() {
                return 0;
            }
        };
        Circle circle = new Circle ("yellow", false, 2.4);
        Rectangle rectangle = new Rectangle("blue", true, 2, 5);
        Square square = new Square();
        System.out.println(circle.getArea());
        System.out.println(circle.getPerimeter());
        System.out.println(circle.toString());
        System.out.println(rectangle.getArea());
        System.out.println(rectangle.getPerimeter());
        System.out.println(rectangle.toString());
        System.out.println(square.toString());
        System.out.println(square.getArea());
        System.out.println(square.getPerimeter());
        System.out.println("___________________________________________");
        System.out.println("--------------Statement 1------------------");
        Shape s1 = new Circle(5.5, "red", false);
        // there is was an error, which occurred due to the lack of a constructor in the Circle class
        System.out.println(s1); // which version?
        System.out.println(s1.getArea()); // which version?
        System.out.println(s1.getPerimeter()); // which version?
        System.out.println(s1.getColor());
        System.out.println(s1.isFilled());
        System.out.println(((Circle) s1).getRadius());
        // there is was error, which occurred due to the lack of getRadius method in Shape class.
        // It was solved by casting
        System.out.println("___________________________________________");
        System.out.println("---------------Statement 2-----------------");
        Circle c1 = (Circle)s1; // Downcast back to Circle
        System.out.println(c1);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());
        System.out.println(c1.getColor());
        System.out.println(c1.isFilled());
        System.out.println(c1.getRadius());
        System.out.println("___________________________________________");
        System.out.println("---------------Statement 3-----------------");
        Shape s3 = new Rectangle(1.0, 2.0, "red", false);
        // there is was an error, which occurred due to the lack of a constructor in the Rectangle class
        System.out.println(s3);
        System.out.println(s3.getArea());
        System.out.println(s3.getPerimeter());
        System.out.println(s3.getColor());
        System.out.println(((Rectangle) s3).getLength());
        // there is was error, which occurred due to the lack of getLength method in Shape class.
        // It was solved by casting
        System.out.println("___________________________________________");
        System.out.println("---------------Statement 4-----------------");
        Rectangle r1 = (Rectangle)s3; // downcast
        System.out.println(r1);
        System.out.println(r1.getArea());
        System.out.println(r1.getColor());
        System.out.println(r1.getLength());
        System.out.println("___________________________________________");
        System.out.println("---------------Statement 5-----------------");
        Shape s4 = new Square(6.6);
        System.out.println(s4);
        System.out.println(s4.getArea());
        System.out.println(s4.getColor());
        System.out.println(((Square) s4).getSide());
        // there is was error, which occurred due to the lack of getSide method in Shape class.
        // It was solved by casting
        System.out.println("___________________________________________");
        System.out.println("---------------Statement 6-----------------");
        Rectangle r2 = (Rectangle)s4;
        System.out.println(r2);
        System.out.println(r2.getArea());
        System.out.println(r2.getColor());
        System.out.println(r2.getSide());
        //there is was error, which occurred due to the lack of abstract method in the Shape class
        System.out.println(r2.getLength());
        System.out.println("___________________________________________");
        System.out.println("---------------Statement 7-----------------");
        Square sq1 = (Square)r2;
        System.out.println(sq1);
        System.out.println(sq1.getArea());
        System.out.println(sq1.getColor());
        System.out.println(sq1.getSide());
        System.out.println(sq1.getLength());
    }



}
