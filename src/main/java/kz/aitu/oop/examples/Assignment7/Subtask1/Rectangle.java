package kz.aitu.oop.examples.Assignment7.Subtask1;

public class Rectangle extends Shape {
    private double width = 1.0;
    private double length = 1.0;

    public Rectangle() {
        super();
        width = 1.0;
        length = 1.0;
    }

    public Rectangle(double width, double length) {
        super();
        this.width = width;
        this.length = length;
    }

    public Rectangle(String color, boolean filled, double width, double length) {
        super(color, filled);
        this.width = width;
        this.length = length;
    }

    public Rectangle(double width, double length, String color, boolean filled) {
        this.width = width;
        this.length = length;
        this.color = color;
        this.filled = filled;
    }


    public double getWidth() {

        return width;
    }

    public void setWidth() {

        this.width = width;
    }

    public double getLength() {

        return length;
    }

    public void setLength() {

        this.length = length;
    }

    @Override
    public double getArea() {
        double area = width * length;
        return area;
    }

    @Override
    public double getPerimeter() {
        double perimeter = (width + length) * 2;
        return perimeter;
    }

    @Override
    public double getSide() {
        return 0;
    }

    @Override
    public String toString() {
        return "A Rectangle with width = " + width + " and length = " + length + " which is a subclass of " + super.toString();
    }

}
