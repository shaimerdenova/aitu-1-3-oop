package kz.aitu.oop.examples.Assignment7.Subtask1;

public class Circle extends Shape {
    private double radius = 1.0;

    public Circle() {
        super();
        radius = 1.0;
    }

    public Circle(double radius) {
        super();
        this.radius = radius;
    }

    public Circle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    public Circle(double radius, String color, boolean filled) {
        this.radius= radius;
        this.color = color;
        this.filled = filled;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }


    @Override
    public String toString() {
        return "A Circle with radius = " + radius + " which is a subclass of " + super.toString();
    }

    @Override
    public double getArea (){
        double pi = 3.14;
        double area;
        area = pi * radius * radius;
        return area;
    }

    @Override
    public double getPerimeter(){
        double pi = 3.14;
        double length;
        length = 2 * pi * radius;
        return length;
    }

    @Override
    public double getSide() {
        return 0;
    }


}
