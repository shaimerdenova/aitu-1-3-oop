package kz.aitu.oop.examples.Assignment7.Subtask2;

public class TestInterface {
    public static void main(String[] args) {
        MovablePoint m1 = new MovablePoint(4, 2,3, 5) {
            @Override
            public void moveUp() {
            }

            @Override
            public void moveDown() {
            }
            @Override
            public void moveRight() {
            }
            @Override
            public void moveLeft() {
            }
        };
        System.out.println(m1.toString());
        MovableCircle m2 = new MovableCircle(4, 2, 3, 5, 2);
        System.out.println(m2.toString());
        m2.moveUp();
        m2.moveDown();
        m2.moveRight();
        m2.moveLeft();

    }
}
