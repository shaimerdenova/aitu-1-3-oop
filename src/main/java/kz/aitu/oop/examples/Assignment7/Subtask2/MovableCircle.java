package kz.aitu.oop.examples.Assignment7.Subtask2;

public class MovableCircle extends MovablePoint implements Movable {
    private int radius;
    private MovablePoint center;
    
    public MovableCircle(int x, int y, int xspeed, int yspeed, int radius){
        super(x, y, xspeed, yspeed);
        this.radius=radius;
    }

    @Override
    public String toString(){
        return "This is override from "+ super.toString();
    }
    @Override
    public void moveUp(){
        System.out.println("Moving up");
    }
    @Override
    public void moveDown(){
        System.out.println("Moving down");
    }
    @Override
    public void moveRight(){
        System.out.println("Moving right");
    }
    @Override
    public void moveLeft(){
        System.out.println("Moving left");
    }

}
