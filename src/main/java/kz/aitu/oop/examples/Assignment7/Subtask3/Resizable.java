package kz.aitu.oop.examples.Assignment7.Subtask3;

public interface Resizable {
    public void resize(int percent);

}
