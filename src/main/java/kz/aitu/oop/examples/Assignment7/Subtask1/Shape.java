package kz.aitu.oop.examples.Assignment7.Subtask1;

public abstract class Shape {
    protected String color="red";
    protected boolean filled = true;

    public Shape() {
        color="green";
        filled= true;
    }


    public Shape(String color, boolean filled){
        this.color = color;
        this.filled = filled;
    }

    public String getColor(){

        return color;
    }

    public void setColor(String color)
    {
        this.color=color;
    }

    public boolean isFilled(){

        return filled;
    }

    public void getFilled(boolean filled){

        this.filled= filled;
    }

    public String toString(){
        if (filled==true){
            return "A Shape with color of "+color + " and filled";
        }
        else {
            return "A Shape with color of "+color + " and not filled";
        }
    }

    public abstract double getArea();
    public abstract double getPerimeter();
    public abstract double getSide();
}


