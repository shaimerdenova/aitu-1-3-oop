package kz.aitu.oop.examples.Assignment7.Subtask3;

public interface GeometricObject {
    public  double getPerimeter();
    public  double getArea();

}
