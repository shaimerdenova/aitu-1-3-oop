package kz.aitu.oop.examples.Assignment7.Subtask3;

public class Circle implements GeometricObject {
    private double radius = 1.0;
    public double getRadius(){
        return radius;
    }
    public void setRadius(){
        this.radius=radius;
    }
    public Circle(double radius){
        this.radius=radius;
    }
    public String toString(){
        return "Circle";
    }
    @Override
    public double getPerimeter(){
        double perimeter = 2*3.14*radius;
        return perimeter;
    }

    @Override
    public double getArea(){
        double area = 3.14*radius*radius;
        return area;
    }


}
