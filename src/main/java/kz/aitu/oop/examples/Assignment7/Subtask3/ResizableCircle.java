package kz.aitu.oop.examples.Assignment7.Subtask3;

import kz.aitu.oop.examples.Assignment7.Subtask3.Circle;
import kz.aitu.oop.examples.Assignment7.Subtask3.Resizable;

public class ResizableCircle extends Circle implements Resizable {
    public ResizableCircle(double radius){
        super(radius);
    }
    @Override
    public String toString(){
        return "This is Resizable Circle override from "+super.toString();
    }
    @Override
    public void resize(int percent){
        double newRadius;
        newRadius = super.getRadius()*percent;
        System.out.println("New radius is equal to "  + newRadius);

    }
}
