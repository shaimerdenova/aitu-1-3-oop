package kz.aitu.oop.examples.Assignment7.Subtask3;

import kz.aitu.oop.examples.Assignment7.Subtask3.Circle;

public class TestCircle {
    public static void main(String[] args) {
        Circle c1 = new Circle(5);
        System.out.println(c1.getRadius());
        System.out.println(c1.toString());
        System.out.println(c1.getPerimeter());
        System.out.println(c1.getArea());

    }
}
