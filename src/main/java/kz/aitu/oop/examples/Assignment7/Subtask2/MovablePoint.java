package kz.aitu.oop.examples.Assignment7.Subtask2;

public class MovablePoint implements Movable {
    private int x;
    private int y;
    private int xspeed;
    private int yspeed;

    public MovablePoint(int x, int y, int xspeed, int yspeed) {
        this.x=x;
        this.y=y;
        this.xspeed=xspeed;
        this.yspeed=yspeed;
    }
    @Override
    public String toString(){

        return "Movable Point";
    }


@Override
    public void moveUp(){
    System.out.println("Moving up");
    }

    @Override
    public void moveDown(){
        System.out.println("Moving down");
    }

    @Override
    public void moveRight(){
        System.out.println("Moving Right");
    }

    @Override
    public void moveLeft(){
        System.out.println("Moving left");
    }
}

