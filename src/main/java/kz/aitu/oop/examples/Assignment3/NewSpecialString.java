package kz.aitu.oop.examples.Assignment3;

public class NewSpecialString {
    String[] values = {"T", "O", "L", "I", "K"};

    // keep the array values internally with/without duplicated values
    public NewSpecialString(String[] values)
    {
        System.out.println(values);
    }


    // return the number of values(strings) that are stored
    public int length(){
        int a=values.length;
        return a;
    }

    // return the value stored at position or -1 if position is not available
    public String valueAt(int position){

        return values[position];
    }

    // return true if value is stored, otherwise false
    public boolean contains(String value){
        for(int i=0; i < values.length; i++){
            if(values[i] == value)
                return true;
        }
        return false;
    }

    // count for how many time value is stored
    public int count(String value){
        int c = 0;
        for(int i=0; i < values.length; i++){
            if(values[i] == value)
                c++;
        }
        return c;
    }

    //print the stored values
    public String[] storedValues(){
        for(int i=0; i < values.length; i++)
            System.out.println(values[i]);
        return values;
    }
}
