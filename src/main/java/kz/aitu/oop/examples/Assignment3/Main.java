package kz.aitu.oop.examples.Assignment3;
import kz.aitu.oop.examples.Assignment3.MyString;
public class Main {
    static int[] values = {1,2,3,4,5};

    public static int length(){
        int a = values.length;
        return a;
    }


    public static int valueAt(int position){
        int b=-1;
        if(values[position] == '\0')
            return b;
        return values[position];
    }


    public static boolean contains(int value){
        for(int i=0; i < values.length; i++){
            if(values[i] == value)
                return true;
        }
        return false;
    }


    public static int count(int value){
        int c = 0;
        for(int i=0; i < values.length; i++){
            if(values[i] == value)
                c++;
        }
        return c;
    }


    public static int[] storedValues(){
        for(int i=0; i < values.length; i++)
            System.out.print(values[i]+" ");
        return values;
    }


    public static void main(String[] args) {
        int l= length();
        System.out.println("Length of the array is " + l);
        int value=valueAt(2);
        System.out.println("Number "+ value+ " on the 2 place");
        boolean result=contains(2);
        System.out.println("Array contains element which value is 2- "+result);
        int num=count(2);
        System.out.println("There is "+num+" elements which are egual to the entered value");
        int[] myArray= storedValues();

    }
}
