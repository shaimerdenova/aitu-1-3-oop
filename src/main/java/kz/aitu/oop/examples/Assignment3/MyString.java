package kz.aitu.oop.examples.Assignment3;

public class MyString {
    int[] values =  {1,2,3,2,4};
    // keep the array values internally
    public MyString(int[] values){
        System.out.println(values);
    }

    // return the number of values that are stored
    public int length(){
        int n = values.length;
        return n;
    }

    // return the value stored at position or -1 if position is not available
    public int valueAt(int position){
        int b=-1;
        if(values[position] == 0)
            return b;
        return values[position];
    }

    // return true if value is stored, otherwise false
    public boolean contains(int value){
        for(int i=0; i < values.length; i++){
            if(values[i] == value)
                return true;
        }
        return false;
    }

    // count for how many time value is stored
    public int count(int value){
        int c = 0;
        for(int i=0; i < values.length; i++){
            if(values[i] == value)
                c++;
        }
        return c;
    }

    //print the stored values
    public int[] storedValues(){
        for(int i=0; i < values.length; i++)
            System.out.print(values[i]+" ");
        return values;
    }

    }