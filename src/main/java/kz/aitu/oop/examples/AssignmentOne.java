package kz.aitu.oop.examples;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AssignmentOne {
    public static void main(String[] args) throws Exception {

        FileReader fileReader = new FileReader();

        System.out.println(fileReader.getFile());

        Shape s1 = new Shape();
        Point a = new Point (2, 4);
        Point b = new Point (3, 0);
        s1.getListOfPoints();
        s1.longestSide();
        s1.calculatePerimeter();
        s1.averageLength();
    }

}

