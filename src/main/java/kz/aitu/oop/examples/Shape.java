package kz.aitu.oop.examples;
import java.util.List;
public class Shape {
    private List<Point> ListOfPoints;

    public Shape(){
    }

    public List<Point> addPoint(List<Point> newPoint) {
        ListOfPoints = newPoint;
        return ListOfPoints;
    }


    public List<Point> getListOfPoints(){
        return ListOfPoints;
    }

    public double calculatePerimeter(){
        double p = 0;
        for (int i=0; i<ListOfPoints.size()-1; i++){
            Point a = ListOfPoints.get(i);
            Point b = ListOfPoints.get(i+1);
            p= p+a.distance(b);
        }
        return p;
    }

    public double longestSide(){
        double longestSide=0;
        double c =0;
        for (int i=0;i<ListOfPoints.size()-1;i++){
            Point a = ListOfPoints.get(i);
            Point b = ListOfPoints.get(i+1);
            c=a.distance(b);
            if(longestSide>c){
                longestSide = c;
            }
        }
        return longestSide;
    }

    public double averageLength(){
        double sum = 0;
        double AVG;
        for (int i=0;i<ListOfPoints.size()-1;i++){
            Point a = ListOfPoints.get(i);
            Point b = ListOfPoints.get(i+1);
            sum = sum + a.distance(b);
        }
        AVG = sum / ListOfPoints.size()+1;
        return AVG;
    }
}
