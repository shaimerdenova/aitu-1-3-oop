package kz.aitu.oop.examples.practice5;

public class task2 {
    static boolean fileExist(String s){
        try {
            String myString = "Tolik";
            if (s == myString) {
                System.out.println("This file exists!");
            }
            else{
                    System.out.println("This file does not exist!");
                }
            return true;
            }
        catch(Exception e){
            return false;
        }
    }

    static boolean isInt(String s){
        try{
            Integer.parseInt(s);
            System.out.println("The string has integer value!");
            return true;
        }
        catch (NumberFormatException e) {
            System.out.println("The string has not integer value!");
            return false;
        }
    }

    static boolean isDouble(String s){
        try {
              Double.parseDouble(s);
            System.out.println("The string has double value!");
            return true;
        }
        catch (Exception e){
            System.out.println("The string has not double value!");
            return false;
        }
    }

    public static void main(String[] args) {
        fileExist("Tolik");
        fileExist("neTolik");
        isInt("ddd");
        isInt("25");
        isDouble("2.3");
        isDouble("sss");
    }

}
