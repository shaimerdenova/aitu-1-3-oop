package kz.aitu.oop.examples.Assignment5;

public class Circle extends Shape{
    private double radius = 1.0;

    public Circle() {
        super();
        radius = 1.0;
    }

    public Circle(double radius){
        super();
        this.radius=radius;
    }

    public Circle(String color, boolean filled, double radius){
        super(color, filled);
        this.radius = radius;
    }
    public double getRadius(){
        return radius;
    }

    public void setRadius(double radius){
        this.radius = radius;
    }

    public double getArea(){
        double pi = 3.14;
        double area;
        area= pi*radius*radius;
        return area;
    }

    public double getLength(){
        double pi = 3.14;
        double length;
        length = 2*pi*radius;
        return length;
    }

    @Override
    public String toString() {
        return "A Circle with radius = "+ radius + " which is a subclass of "+ super.toString();
    }

}
