package kz.aitu.oop.examples.Assignment5;

public class Square extends Rectangle {
    double side;
    public Square(){
        super();
        side = 1.0;
    }

    public Square(double side){
        super();
        this.side=side;
    }

    public Square(String color, boolean filled, double width, double length, double side) {
        super(color, filled, width, length);
        this.side = side;
    }

    public double getSide(){
        return side;
    }

    public void setSide(double side){
        this.side = side;
    }

    public void setWidth(double side){
        this.side = side;
    }

    public void setLength(double side){
        this.side = side;
    }

    @Override
    public String toString() {
        return "A Square with side = " + side + " which is a subclass of "+ super.toString();
    }
    @Override
    public double getArea(){
        return super.getArea();
    }

    @Override
    public double getPerimeter(){
        return super.getPerimeter();
    }

    @Override
    public void setWidth(){
        super.setWidth();
    }

    public void setLength(){
        super.setLength();
    }
}
