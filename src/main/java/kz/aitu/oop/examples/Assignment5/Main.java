package kz.aitu.oop.examples.Assignment5;

public class Main {
        public static void main(String[] args) {
            Shape shape = new Shape("red", true);
            Shape shape2 = new Shape ();
            Circle circle1 = new Circle();
            Circle circle2 = new Circle(3.0);
            Circle circle = new Circle ("yellow", false, 2.4);
            Rectangle rectangle1 = new Rectangle();
            Rectangle rectangle2 = new Rectangle(3, 4);
            Rectangle rectangle = new Rectangle("blue", true, 2, 5);
            Square square = new Square();
            Square square2 = new Square(5);
            System.out.println(shape.getColor());
            System.out.println(shape.isFilled());
            System.out.println(shape.toString());
            System.out.println(circle.getArea());
            System.out.println(circle.getLength());
            System.out.println(circle.toString());
            System.out.println(rectangle.getArea());
            System.out.println(rectangle.getPerimeter());
            System.out.println(rectangle.toString());
            System.out.println(square.toString());
            System.out.println(square.getArea());
            System.out.println(square.getPerimeter());
            System.out.println(square.getLength());
            System.out.println(square.getWidth());

        }
    }
